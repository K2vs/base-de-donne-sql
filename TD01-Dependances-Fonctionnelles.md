# [TD01: Dépendances fonctionnelles](https://poesi.esi-bru.be/pluginfile.php/62/course/section/383/Labo1-D%C3%A9pendancesFonctionnelles.pdf)
Cours pratique de l'École Supérieure d’Informatique (HE2B ESI) sur les TD en persistance de données.
## Exercice 1 :
1. profId -> coursId : "profId détermine fonctionnellement coursID" et donc cela signifie que pour un profId à tout moment il n'y a qu'un seul coursId ce qui est vrai, car un professeur ne peut pas se démultiplier.

## Exercice 2 :
{A, B} -> {C, D} où A = 'abc' && B = 'xyz' && C = 'rst' && D = '123'
1. ~~Puisque le cas ne change pas et que {A, B} détermine {C, D} ; on sait qu'ils seront toujours liés et identiques ; donc c'est vrai.~~

    Non car à tout moment AB détermine CD ne veut pas dire que les valeurs ne peuvent changer. Prenons l’exemple où AB serait ’CUV”local 003’ un prof dans un local et CD ’34 étudiants’ ’12 étudiantes’ le nombre d’étudiants et d’étudiantes devant lui. Ces nombres d’étudiants peuvent varier dans le temps mais à tout moment il ne peut avoir qu’une seule valeur pour ces deux nombres pour un prof et un local.

2. ~~Même cas qu'au-dessus, sauf qu'il n'y a plus la limitation de temps, mais ça reste vrai.~~

    Oui, à un moment donné si deux tuples ont les mêmes valeurs de AB ils auront obligatoirement les mêmes valeurs de CD. Mais attention, comme dans une relation on ne peut avoir deux tuples identiques, les valeurs de E, F et G devront donc être différentes.

3. C'est {A, B} qui détermine {C, D}, pas l'inverse donc c'est faux.
2. ~~C'est vrai, car dans une relation il ne peut pas avoir de ligne vide ni de duplication de ligne. Par conséquent, si le nombre de lignes de R[A,B] n'est pas égal à celui de R[C,D] alors dans R(A, B, C, D) il y aura des lignes vides (ce qui n'est pas possible).~~

    Non, car nous pouvons avoir AB → CD et A’B’ → CD. Par exemple si NuméroEtudiant → NomEtudiant, nous pouvons avoir 99999 → Paulus et 88888 → Paulus, deux étudiants ayant le même nom.

## Exercice 3 :
{A, B} est clé primaire dans R(A, B, C, D, E) :
1. ~~A ne peut pas déterminer B, car A et B seraient égaux et donc le couple {A, B} n'aurait pas de sens : faux.~~

    Non car AB est une clé primaire et donc c’est un identifiant minimal. Si A → B cela voudrait dire que seul A permettrait d’identifier un tuple de la relation et que donc AB ne serait pas un identifiant minimal
2. On ne peut pas savoir si A détermine C ; mais il est fort probable que ça ne soit pas suffisant d'où la clé primaire {A, B} : faux.
3. A et B doivent ne pas être nulls pour être une clé primaire : vrai.
4. ~~Ce n'est pas D qui détermine {A,B}, mais l'inverse : faux.~~

    Possible mais nous n’avons pas assez d’informations pour affirmer oui ou non. Si D → A,B alors D est une clé candidate non choisie comme primaire.
5. ~~{A,B} pourrait déterminer le couple {C,D} même si ça n'a pas beaucoup de sens puisque ça dupliquerait des lignes : vrai.~~

    Oui, A,B → C,D,E donc A,B → C,D

## Exercice 4 :
1. Oui car DptNo identifie un département.
2. Non car Deux employés peuvent avoir le même nom.
3. Non car on peut imaginer un même manager pour deux départements.
4. Non car dans un département il y aura plusieurs employés.
5. Oui car EmpNo identifie un employé.
6. ~~Non car un employé ne défini pas un département~~

    Oui car EmpDpt identifie un département
7. Oui car EmpNo identifie un employé
8. Oui car DptMgr identifie un employé
9. Oui car DptNo identifie un département et EmpNo identifie un employé
10. ~~Non car ce sont deux choses différentes~~

    Oui car DptAdm identifie un département