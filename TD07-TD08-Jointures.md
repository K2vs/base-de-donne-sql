# [TD07-TD08: Jointure](https://poesi.esi-bru.be/pluginfile.php/13747/mod_page/content/4/Labo78-Jointures.pdf)
Cours pratique de l'École Supérieure d’Informatique (HE2B ESI) sur les TD en persistance de données.
## Exercice 1 :
1.  
    - MEDECIN :
        - PRIMAIRE : MedNo
    - APOUR :
        - PRIMAIRE : {ApoMed, ApoSpe}
        - ETRANGER : ApoMed, ApoSpe
    - SEPCIALISTE : {SpeNo}
    - VISITE :
        - PRIMAIRE : VisNo
        - ETRANGER : VisPat, VisMed
    - PATIENT :
         - PRIMAIRE : PatNo
    - ELTORDONNANCE :
        - PRIMAIRE : {EltNo, EltVis}, {EltVis, EltMed}
        - ETRANGER : EltVis, EltMed
    - MEDICAMENT :
        - PRIMAIRE : MdcNo
        - ETRANGER : MdcInit
    - CONTRINDIC
        - PRIMAIRE : {CntMdcA, CntMdcB}
        - ETRANGER : CntMdcA, CntMdcB
2. 
    1. Non, il faut un EltVis.
    2. Oui, il peut y avoir sur une même ordonnance plusieurs médicaments : {EltVis, EltMed}.
    3. Oui, un médecin n'a pas besoin de visite pour exister.
    4. Oui, le médecin n'a aucune clé étrangère.
    5. Oui, une visite peut ne pas avoir d'ordonnance (aucune clé primaire n'en dépend).
    6. Non, il y a une clé primaire {EltVis, EltMed} ce qui rend pour une visite un seul couple avec un médicament.

3. 
   1. Elle est correct.
   2. Elle est correct
   3. Elle est incorrect car après un join, on ne peut pas refaire un from.
   4. Elle est correct.
   5. Elle est correct.

## Exercice 2 :
__**! A FAIRE EN ALGEBRE RELATIONNEL !**__
1. select distinct nom from gcuv.client
    join gcuv.commande
    on client.ncli = commande.ncli;
2. select distinct nom from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where produit.npro = 'XXX';
3. select distinct nom from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where produit.prix > 100;
4. select distinct localite from gcuv.client where compte <= 0;
5. [?????] select distinct localite from gcuv.client where compte = 0 and exists(select compte from gcuv.client where compte < 0);

## Exercice 3 :

1. select ncom, nom from gcuv.client
    join gcuv.commande on commande.ncli = client.ncli;
2. select commande.ncom, libelle, qcom, prix from gcuv.detail
    join gcuv.produit on detail.npro = produit.npro
    join gcuv.commande on commande.ncom = detail.ncom;
3. select commande.ncom, datecom, libelle, qcom, prix from gcuv.detail
    join gcuv.produit on detail.npro = produit.npro
    join gcuv.commande on commande.ncom = detail.ncom;
4.  select commande.ncom, nom, adresse, localite, libelle, prix, qcom from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where commande.ncom = 30188;
5. select commande.ncom, nom, adresse, localite, libelle, prix, qcom, qcom*prix somme from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where commande.ncom = 30188;
6. select sum(qcom*prix) somme from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    where commande.ncom = 30188;
7. select localite from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where produit.npro = 'CS464';
8. select distinct libelle, produit.npro from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    where libelle like '%SAPIN%'
9. select distinct libelle, produit.npro from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where libelle like '%SAPIN%' and client.localite = 'Toulouse'
10. select distinct localite from gcuv.client
    join gcuv.commande on commande.ncom = commande.ncom
    where datecom LIKE '%/12/00%'
11. select distinct nom, adresse, localite from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.client on client.ncli = commande.ncli
    where produit.libelle like '%POINTE%' and produit.libelle like '%ACIER%';
12. select client.ncli, nom from gcuv.client
    join gcuv.commande on commande.ncom = commande.ncom
    where datecom LIKE '%/01' and compte < 0;
13. select distinct detail.npro from gcuv.detail
    join gcuv.commande on commande.ncom = detail.ncom
    where datecom LIKE '%/01/01'
14. select ncom from gcuv.commande
    join gcuv.client on client.ncli = commande.ncli
    where localite in('Namur', 'Bruxelles');
15. select ncli, nom from gcuv.client
    where adresse like '%bouvlard%' or adresse like '%bvd%' or adresse like '%bld%' or adresse like '%bd%';
16.  select distinct produit.npro, libelle from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    where qcom > 50;
17. select distinct produit.npro, libelle from gcuv.detail
    join gcuv.produit on produit.npro = detail.npro
    join gcuv.commande on commande.ncom = detail.ncom
    where qcom*prix > 5000;
18. select max(qstock) from gcuv.produit;
19. select max(qstock) from gcuv.detail
    join gcuv.commande on commande.ncom = detail.ncom
    join gcuv.produit on produit.npro = detail.npro