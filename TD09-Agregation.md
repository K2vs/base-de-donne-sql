# [TD09: Agrégation](https://poesi.esi-bru.be/pluginfile.php/13812/mod_page/content/8/Labo9-Agregation.pdf)
Cours pratique de l'École Supérieure d’Informatique (HE2B ESI) sur les TD en persistance de données.
## Exercice 1 :
1. Donné dans l'énoncé

2. Inutile car on groupe par clé primaire.
3. Inutile car on groupe par clé primaire.
4.  "C01", "F", "110200"

    "C01", "M", "110300"

    "D21", "M", "140200"

    "E11", "F", "100200"

    "E11", "M", "100800"
5. 8
6.  "A00", "2"

    "C01", "6"

    "D11", "6"

    "D21", "5"

    "E11", "6"

    "E21", "5"
7. Impossible car dptmgr n'existe pas dans le select
8.  "030","1"

    "340","1"

    "060","2"

    "070","1"

    "100","2"
9. Impossible de prendre empdpt vu qu'il y a une selection sur la colone empsexe.
10. Aucune donnée n'est supérieur à 3 (2 chacuns car multiplication cartésienne).

## Exercice 2 :
1. ```select empno, empnom, empsexe, dptlib, dptmgr from gcuv.employe
   join gcuv.departement on departement.dptno = employe.empdpt
   order by empsexe, dptlib, dptmgr;```
2. ```select sum(empsal), empsexe from gcuv.employe group by empsexe;```
3. ```select sum(empsal), dptlib from gcuv.employe
   join gcuv.departement on departement.dptno = employe.empdpt
   group by dptno, dptlib
   order by dptlib;```
4. ```select dptno, empsexe, count(*) from gcuv.employe
   join gcuv.departement on departement.dptno = employe.empdpt
   where empsexe = 'F'
   group by empsexe, dptno;```
5. ```select dptlib, sum(empsal) from gcuv.employe
   join gcuv.departement on departement.dptno = employe.empdpt
   group by dptlib
   having sum(empsal) > 85000;```
6. ```select dptlib from gcuv.employe
   join gcuv.departement on departement.dptno = employe.empdpt
   group by dptlib, empsexe
   having count(*) > 3 and empsexe = 'M'
   order by count(*) DESC;```
7. ```select count(dptadm) from gcuv.departement where dptadm is not null;```
8. ```select count(distinct(dptadm)) from gcuv.departement where dptadm is not null;```
9. ```select empDpt, count(*) from gcuv.employe
   join gcuv.departement on departement.dptMgr = employe.empNo
   group by empDpt
   having  count(*) > 3;```
10. ```select dptNo, max(empSal), avg(empSal), min(empSal) from gcuv.employe
   join gcuv.departement on departement.dptno = employe.empdpt
   where dptMgr != empNo
   group by dptNo;```