# [TD04: Fonctions](https://poesi.esi-bru.be/pluginfile.php/62/course/section/383/Labo4-Fonctions%20%281%29.pdf)
Cours pratique de l'École Supérieure d’Informatique (HE2B ESI) sur les TD en persistance de données.
## Exercice 1 :
1. select count(ncli) from client;
2. select count(ncli) from gcuv.client where cat is not null;
3. select count(cat) from gcuv.client;
4. select count(ncom) from gcuv.commande where ncli = 'C400';
5. select count(ncom) from gcuv.commande where ncli = 'C401';
6. select count(distinct cat) from gcuv.client;
7. select count(ncli) from gcuv.client where cat is null;
8. select max(prix) from gcuv.produit;
9. select min(prix) from gcuv.produit;
10. select avg(prix) from gcuv.produit;
11. select sum(prix) from gcuv.produit;
12. select sum(prix*qstock) from gcuv.produit;
13. select min(datecom) from gcuv.commande;
14. select sum(compte), min(compte), avg(compte), max(compte) from gcuv.client;
15. select sum(compte), min(compte), avg(compte), max(compte) from gcuv.client where localite = 'Toulouse';
16. select nom, upper(adresse || localite) totalAdresse from gcuv.client; **ou** select nom, upper(concat(adresse, localite)) totalAdresse from gcuv.client;
17. select qstock*prix || ' euros' valeurStock from gcuv.produit where npro = 'CS264';
18. select count(npro) from gcuv.produit;
19. select count(distinct npro) from gcuv.detail;
20. select sum(qcom) from gcuv.detail;
21. select max(qcom), count(ncom) from gcuv.detail where npro = 'CS464';
22. select count(npro) from gcuv.produit where prix < 100;
23. select npro, libelle from gcuv.produit where prix*qstock between 40000 and 100000;

## Exercice 2 :
1. La plus petite et la plus grande quantité de produit commandée, parmi toutes les commandes dont le numéro est entre 30180 et 30186 (bornes inclues).
2. Le nombre de produits dont la valeur du stock (en euros) est inférieur ou égal à 100000.
3. Le nombre de clients qui ont fait une commande avant le 1er janvier 2009.
4. Le nombre de clients différents qui ont commandé avant le 1er janvier 2009.