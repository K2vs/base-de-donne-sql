# [TD02: Schéma conceptuel](https://poesi.esi-bru.be/pluginfile.php/62/course/section/383/Labo2-SchemaConceptuel.pdf)
Cours pratique de l'École Supérieure d’Informatique (HE2B ESI) sur les TD en persistance de données.
## Exercice 1 :
1. Avec la clé primaire " EmpNo " qui est unique et toutes ses variantes {EmpNo, EmpNom}, {EmpNo, EmpSal}, ...

2. Oui, car EmpDpt n'est pas optionnel.
3. Non, car il n'y a qu'une seule ligne d'employé et qu'un seul champ pour indiquer l'EmpDpt.
4. ~~Oui, il faut un DptMgr qui pointe vers un employé.~~

    Non, c’est seulement EmpDpt qui indique dans quel département est un employé. Il se pourrait donc qu’un département ne soit jamais repris dans les valeurs de la colonne EmpDpt.
5. Non, il n'y a aucune contrainte sur l'EmpDpt.
6. Oui, puisque DptMgr cherche un EmpNo qui est donc employé de l'entreprise.
7. Oui, il peut y avoir plusieurs départements avec le même DptAdm
8. Oui, il suffit de ne pas mettre le DptNo de ce département dans le DptAdm du second.
9. Oui, DptAdm peut être facultatif
10. Même si ça n'a pas beaucoup de sens, c'est possible si le DptAdm est égal au DptNo courant.

## Exercice 2 :
1.  
    - Oui, car StaCentre n'est pas une clé primaire de Stage.
    - ~~Non, car pour trouver un moniteur il faut se baser sur l'habilitation qui se base elle-même sur le sport demandé et sur le moniteur.~~
    
        Oui, imaginons : Piet (id 007) est moniteur, dans la table Habilitation il existe une ligne (007, Foot) pour dire qu’il connaît le football. Il n’y a pas d’autre ligne pour Piet dans Habilitation. Dans la table Anime il existe une ligne (007, sta1) pour dire qu’il anime le stage sta1. Le stage sta1 est un stage de natation synchronisée.
    - Non, car participation est composé d'une clé primaire composée {parMbr, parStage} et donc elle ne peut être qu'unique avec le Stage et l'id d'un membre.
2. ~~Parce qu'avec la même date de début et le même centre il pourrait y avoir plusieurs montants, plusieurs prix, plusieurs sports, etc. Et donc ça ne serait pas l'unique façon de sélectionner 1 Stage.~~

    Si (staCentre, staDateDeb) serait une clé primaire, il ne pourrait pas avoir deux fois la même valeur pour ce couple dans la table. Donc il ne pourrait pas avoir deux stages débutants à la même date dans un même centre. Ceci arrive pourtant très souvent en début de vacances scolaires.
3. Un moniteur ne pourra être assigné qu'à un seul et unique Stage.

    (Par contre il ne sera plus possible d’affecter un même moniteur à deux stages au même moment.)