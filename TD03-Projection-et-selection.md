# [TD03: Projection - Sélection](https://poesi.esi-bru.be/pluginfile.php/62/course/section/383/Labo3-ProjectionSelection.pdf)
Cours pratique de l'École Supérieure d’Informatique (HE2B ESI) sur les TD en persistance de données.
## Exercice 1 :
1. select npro, prix from produit;
2. ~~select * from gcuv.detail where qcom > 500;~~

    select ncomn,npro, qcom from gcuv.detail where qcom > 500;
3. select distinct ncom from gcuv.detail where ncom > 30185;
4. select distinct nom from gcuv.client where cat is null;
5. select npro, libelle from gcuv.produit where qstock = 0;

## Exercice 2 :
1. select * from gcuv.produit;
2. ~~select distinct prix, libelle from gcuv.produit;~~

    select prix, libelle from gcuv.produit;
3. ~~select distinct prix, libelle from gcuv.produit where prix > 200;~~

    select prix, libelle from gcuv.produit where prix > 200;
4. select ncli, nom, localite from gcuv.client where cat = 'C1' and localite != 'Toulouse';
5. select * from gcuv.produit where libelle like '%ACIER%';
6. select distinct localite from gcuv.client;
7. ~~select distinct cat from gcuv.client where localite = 'Toulouse' and cat is not null;~~

    select distinct cat from gcuv.client where localite = 'Toulouse';
8. select npro, libelle from gcuv.produit where prix between 100 and 150;
9. select npro, libelle from gcuv.produit where prix > 100 and prix < 150;
10. ~~select ncli, nom, compte from gcuv.client where (localite = 'Poitiers' or  localite = 'Bruxelles') and compte >= 0;~~

    select ncli, nom, compte from gcuv.client where localite in('Poitiers','Bruxelles') and compte >= 0;
11. select ncli, nom, localite from gcuv.client where nom < localite;
12. select * from gcuv.client where localite in('Lille','Namur');
13. select * from gcuv.client where localite not in('Lille','Namur');
14. select * from gcuv.client where localite = 'Namur' and cat = 'C1';
15. select * from gcuv.client where localite = 'Namur' or cat = 'C1';
16. select * from gcuv.client where localite != 'Namur' and cat = 'C1';
17. ~~select * from gcuv.client where cat != 'C1' or localite != 'Namur';~~

    select * from gcuv.client where cat != 'C1' or localite = 'Namur';
18. select * from gcuv.client where cat in('B1', 'C1') or localite in('Namur', 'Lille');
19. select * from gcuv.client where (cat in('B1', 'C1') and localite not in ('Namur', 'Lille')) or (cat not in('B1', 'C1') and localite in('Namur', 'Lille'));
20. select * from gcuv.client where cat in('B1', 'C1') and localite in('Lille', 'Namur');
21. ~~select * from gcuv.client where (cat != 'B1' and cat != 'C1') and (localite != 'Lille' and localite != 'Namur');~~

    select * from gcuv.client where (cat not in('B1','C1') and (localite not in('Lille','Namur');